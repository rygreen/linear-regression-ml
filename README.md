# Linear Regression-ML

Module 2 EART-219 Manager-Developer Assignment

For this interview, I am asking our developer inverviewee, Ryan Anderson, to accomplish 4 tasks:

1. Clone the Linear Regression-ML repo

2. Create a 'dev' branch

3. On the dev branch, create two new training datasets and plot your results. Please include x and y labels in addition to a title. Add these changes in at least 2 different commits.

4. Finally, push your new changes and submit a merge request that I can then evaluate.

Good luck!

